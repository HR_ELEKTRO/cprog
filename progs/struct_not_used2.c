#include <stdio.h>

typedef struct
{
    double x;
    double y;
} punt_t;

void verwissel_x_en_y(punt_t *p)
{
    int hulp = (*p).x;
    (*p).x = (*p).y;
    (*p).y = hulp;
}

int main(void)
{
    punt_t punt = {3.4, 7.6};
    printf("(%f,%f)\n", punt.x, punt.y);
    verwissel_x_en_y(&punt);
    printf("(%f,%f)\n", punt.x, punt.y);
    return 0;
}

#include <stdio.h>

int main()
{
    int histogram[] = {4, 2, 1, 3, 8, 14, 9, 7, 3, 2};
    int totaal = 0, aantal_resultaten = 0;
    for (size_t i = 0; i < sizeof histogram / sizeof histogram[0]; i++)
    {
        totaal += histogram[i] * (i + 1);
        aantal_resultaten += histogram[i];
    }
    printf("Het gemiddeld behaalde resultaat = %f\n", (double) totaal / aantal_resultaten);
    return 0;
}
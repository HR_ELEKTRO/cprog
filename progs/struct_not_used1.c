#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct
{
    int snelheid;
    char richting[4];
} wind_t;

typedef struct
{
    float temperatuur;
    wind_t wind;
} weerdata_t;

weerdata_t meet(void);
void sorteer_metingen_op_windsnelheid(weerdata_t meting[], size_t aantal_metingen);
void print_weer(weerdata_t w);

weerdata_t meet_weer(void)
{
    // dummy implementatie bij gebrek aan echte sensors
    weerdata_t w = {rand() % 410 / 10.0, {rand() % 126, "NO"}};
    return w;
}

void wissel(weerdata_t *pw1, weerdata_t *pw2)
{
    weerdata_t hulp = *pw1;
    *pw1 = *pw2;
    *pw2 = hulp;
}

void sorteer_metingen_op_windsnelheid(weerdata_t meting[], size_t aantal_metingen)
{
//  insertion sort alleen toepasbaar voor kleine aantallen
    for (size_t i = 1; i < aantal_metingen; i++)
    {
        for (size_t j = i; j > 0 && meting[j - 1].wind.snelheid > meting[j].wind.snelheid; j--)
        {
            wissel(&meting[j - 1], &meting[j]);
        }
    }
}

void print_weer(weerdata_t w)
{
    printf("%4.1f C %3d km/h %s", w.temperatuur, w.wind.snelheid, w.wind.richting);
}

void print_metingen(weerdata_t meting[], size_t aantal_metingen)
{
    for (size_t i = 0; i < aantal_metingen; i++)
    {
        print_weer(meting[i]);
        printf("\n");
    }
}

int main(void)
{
    srand(time(NULL));
    weerdata_t meting[5];
    for (size_t i = 0; i < sizeof meting / sizeof meting[0]; i++)
    {
        meting[i] = meet_weer();
    }
    printf("Weermetingen:\n");
    print_metingen(meting, sizeof meting / sizeof meting[0]);
    printf("Weermetingen gesorteerd op windsnelheid:\n");
    sorteer_metingen_op_windsnelheid(meting, sizeof meting / sizeof meting[0]);
    print_metingen(meting, sizeof meting / sizeof meting[0]);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct wind
{
    int snelheid;
    char richting[4];
} wind_t;

wind_t meet_wind(void);
int bepaal_gemiddelde_windsnelheid(wind_t meting[], size_t aantal_metingen);
void print_wind(wind_t w);
int bepaal_windkracht(int gemiddelde_windsnelheid);

wind_t meet_wind(void)
{
    // dummy implementatie bij gebrek aan een echte sensor
    wind_t w = {rand() % 126, "NO"};
    return w;
}

int bepaal_gemiddelde_windsnelheid(wind_t meting[], size_t aantal_metingen)
{
    // bepaal gemiddelde windsnelheid
    int totaal = 0;
    for (size_t i = 0; i < aantal_metingen; i++)
    {
        totaal += meting[i].snelheid;
    }
    return totaal / aantal_metingen;
}

int bepaal_windkracht(int gemiddelde_windsnelheid)
{
    int max_km_per_uur_voor_Beaufort[] = {1, 5, 11, 19, 28, 38, 49, 61, 74, 88, 102, 117};
    size_t beaufort;
    for (beaufort = 0; beaufort < 12; beaufort++)
    {
        if (gemiddelde_windsnelheid <= max_km_per_uur_voor_Beaufort[beaufort])
        {
            break;
        }
    }
    return beaufort;
}

void print_wind(wind_t w)
{
    printf("%d km/h %s", w.snelheid, w.richting);
}

int main(void)
{
    srand(time(NULL));
    wind_t meting[600];
    printf("De wind is ");
    print_wind(meet_wind());
    printf("\n");
    for (size_t i = 0; i < sizeof meting / sizeof meting[0]; i++)
    {
        meting[i] = meet_wind();
    }
    int g = bepaal_gemiddelde_windsnelheid(meting, sizeof meting / sizeof meting[0]);
    printf("De gemiddelde windsnelheid is %d km/h\n", g);
    int b = bepaal_windkracht(g);
    printf("Dit komt overeen met windkracht %d\n", b);
    return 0;
}

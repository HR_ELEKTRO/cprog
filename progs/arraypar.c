#include <stdio.h>
#include <assert.h>

void print_array(const int a[], size_t aantal) 
{
    assert(aantal > 0);
    for (size_t i = 0; i < aantal-1; i++)
    {
        printf("%d, ", a[i]);
    }
    printf("%d\n", a[aantal-1]);
}

void vul_array_met_kwadraten(int a[], size_t aantal)
{
    for (size_t i = 0; i < aantal; i++)
    {
        a[i] = i * i;
    }
}

int main()
{
    int test_array[] = {0, 1, 2, 3, 4};
    print_array(test_array, sizeof test_array / sizeof test_array[0]);
    vul_array_met_kwadraten(test_array, sizeof test_array / sizeof test_array[0]);
    print_array(test_array, sizeof test_array / sizeof test_array[0]);
    return 0;
}

#include <stdio.h>

int main()
{
    int histogram[] = {4, 2, 1, 3, 8, 14, 9, 7, 3, 2};
    for (size_t i = 0; i < sizeof histogram / sizeof histogram[0]; i++)
    {
        printf("%2zu ", i + 1);
        for (int j = 0; j < histogram[i]; j++)
        {
            printf("*");
        }
        printf("\n");
    }
    return 0;
}
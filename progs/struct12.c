#include <stdio.h>

typedef struct
{
    int snelheid;
    char richting[4];
} wind_t;

void corigeer_meting(wind_t *pw);

int main(void)
{
    wind_t meting = {25, "WNW"};
    printf("%3d km/h %s\n", meting.snelheid, meting.richting);
    corigeer_meting(&meting);
    printf("Na correctie:\n");
    printf("%3d km/h %s\n", meting.snelheid, meting.richting);
    return 0;
}

void corigeer_meting(wind_t *pw)
{
    (*pw).snelheid += 2;
}

#include <stdio.h>

int main()
{
    int windmeting_per_seconde[60];
    int gemeten_wind = 100;
    
    windmeting_per_seconde[-1] = gemeten_wind;
	windmeting_per_seconde[60] = gemeten_wind;
	windmeting_per_seconde[10000] = gemeten_wind;
    windmeting_per_seconde[0] = gemeten_wind;
    
    for (size_t i = 0; i < 60; i++)
    {
        printf("windmeting_per_seconde[%zu] = %d\n", i, windmeting_per_seconde[i]);
    }
    return 0;
}